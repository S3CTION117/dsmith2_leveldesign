﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class light : MonoBehaviour
    
{
    public TriggerListener trigger;
    public Image cursorImage;
    public Light spotLight;
    public AudioSource audioSource;
    private Animation anim; 

    void Start()
    {
        cursorImage.enabled = false;
        audioSource = GetComponent<AudioSource>();
        anim = GetComponent<Animation>();
    }


    void Update()
    {
        Debug.Log("update was called");
    }

    void OnMouseOver()
    {
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled != true)
            {
                cursorImage.enabled = true;
            }
            else
            {
                cursorImage.enabled = false;
            }
        }


    }

    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
        }

    }
    void OnMouseDown()
    {
        if (trigger.playerEntered == true)
        {
            audioSource.Play();
            anim.Stop();
            anim.Play();
            Debug.Log("switch pressed");
            if (spotLight.intensity > 0f)
            {
                spotLight.intensity = 0f;
            }           
            else
            {
                spotLight.intensity = 1.95f;
            }

        }

    }
}


