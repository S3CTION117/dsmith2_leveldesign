﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpeningDoors : MonoBehaviour
{
    public Transform door1tran;
    public Transform door2tran;
    public float openAmount = 1f;


    void Start()
    {
    }

    void Update()
    {
    }

    void OnTriggerEnter(Collider col)

    {
        if (col.gameObject.tag == "Player")

        {
            door1tran.localPosition = new Vector3(openAmount, 0, 0);
            door2tran.localPosition = new Vector3(-openAmount, 0, 0);
        }
    }
    /*
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            door1tran.localPosition = new Vector3.zero;
            door2tran.localPosition = new Vector3.zero;
        }
    }
       
    

    
    
    IEnumerator DoorMove()
        {
        float xpos = door1tran.localPosition.x;
        while (xpos < (openAmount - 0.02f))

    yield return null;
        }
        */
}
     




