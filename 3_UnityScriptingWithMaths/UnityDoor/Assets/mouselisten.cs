﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouselisten : MonoBehaviour {

    public bool mouseCusorOn;
    public bool mouseClicked;

   
    void OnMouseDown ()
    {
        mouseClicked = true;

    }


    void OnMouseUp()
    {
        mouseClicked = false;
    }

    void OnMouseOver()
    {
        if (mouseClicked == false)
        {
            mouseCusorOn = true;
        }
    }

    void OnMouseExit()
    {
            mouseCusorOn = false;
    }
}
