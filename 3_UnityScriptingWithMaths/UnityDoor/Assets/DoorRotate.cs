﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DoorRotate : MonoBehaviour {

    public GameObject door;

    public float targetRot = 90f;
    GameObject myPlayer;
    bool doorOpen;
    public float speed = 1f;
    public mouselisten mouse;
    bool inTrigger;
    bool click;
    public Image cursorImage;

    Vector3 closedRot;


    void Start()
    {
        myPlayer = GameObject.FindGameObjectWithTag("Player");
        closedRot = transform.localRotation.eulerAngles;
    }

    void Update()
    {
        if (inTrigger)
        {
            if (mouse.mouseCusorOn == true)
            {
                if (cursorImage.enabled == false)
                {
                    cursorImage.enabled = true;
                }
            }
            else
            {
                cursorImage.enabled = false;
            }
        }
        if (inTrigger && mouse.mouseClicked && click == false)
        {
            click = true;
            DoorInteract();
        }
        else if (mouse.mouseClicked == false)
        {
            click = false;
        }
        

    }

    void DoorInteract()
    {
        Vector3 finishRot;
        if (!doorOpen)
        {
            Vector3 playerDir = door.transform.position - myPlayer.transform.position;
            float dot = Vector3.Dot(playerDir, transform.forward);
            Debug.Log(dot);
            doorOpen = true;

            if (dot > 0f)
            {
                finishRot = new Vector3(closedRot.x, closedRot.y + targetRot, closedRot.z);
            }
            else
            {
                finishRot = new Vector3(closedRot.x, closedRot.y - targetRot, closedRot.z);
            }
        }
        else
        {
            finishRot = closedRot;
            doorOpen = false;
        }

        StopCoroutine("DoorMotion");
        StartCoroutine("DoorMotion", finishRot);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            inTrigger = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            inTrigger = false;
            cursorImage.enabled = false;

        }
    }

    

    IEnumerator DoorMotion(Vector3 target)
    {
        while(Quaternion.Angle(door.transform.localRotation, Quaternion.Euler(target)) >0.02f)
        {
            door.transform.localRotation = Quaternion.Slerp(door.transform.localRotation, Quaternion.Euler(target), speed * Time.deltaTime);
            yield return null;
        }
        door.transform.localRotation = Quaternion.Euler(target);
        yield return null;

    }
}
