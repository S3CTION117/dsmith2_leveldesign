﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class openingdoors : MonoBehaviour
{
    public Transform door1Tran;
    public Transform door2Tran;
    public float openAmount = 1f;


    void Start()
    {
    }

    void Update()
    {
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            door1Tran.localPosition = new Vector3(openAmount, 0, 0);
            door2Tran.localPosition = new Vector3(-openAmount, 0, 0);
        }
    }
    private void OnTriggerExit(Collider col)
    {
    
    }
}